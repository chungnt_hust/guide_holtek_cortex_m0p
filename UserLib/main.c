/*********************************************************************************************************//**
 * @file    IP/Example/main.c
 * @version $Rev:: 2178         $
 * @date    $Date:: 2020-08-05 #$
 * @brief   Main program.
 *************************************************************************************************************
 * @attention
 *
 * Firmware Disclaimer Information
 *
 * 1. The customer hereby acknowledges and agrees that the program technical documentation, including the
 *    code, which is supplied by Holtek Semiconductor Inc., (hereinafter referred to as "HOLTEK") is the
 *    proprietary and confidential intellectual property of HOLTEK, and is protected by copyright law and
 *    other intellectual property laws.
 *
 * 2. The customer hereby acknowledges and agrees that the program technical documentation, including the
 *    code, is confidential information belonging to HOLTEK, and must not be disclosed to any third parties
 *    other than HOLTEK and the customer.
 *
 * 3. The program technical documentation, including the code, is provided "as is" and for customer reference
 *    only. After delivery by HOLTEK, the customer shall use the program technical documentation, including
 *    the code, at their own risk. HOLTEK disclaims any expressed, implied or statutory warranties, including
 *    the warranties of merchantability, satisfactory quality and fitness for a particular purpose.
 *
 * <h2><center>Copyright (C) Holtek Semiconductor Inc. All rights reserved</center></h2>
 ************************************************************************************************************/
// <<< Use Configuration Wizard in Context Menu >>>

/* Includes ------------------------------------------------------------------------------------------------*/
#include "ht32.h"
#include "ht32_board.h"
#include "uartUser.h"
#include "systemClockCfg.h"
#include "string.h"
/** @addtogroup Project_Template Project Template
  * @{
  */

/** @addtogroup IP_Examples IP
  * @{
  */

/** @addtogroup Example
  * @{
  */


/* Settings ------------------------------------------------------------------------------------------------*/
/* Private types -------------------------------------------------------------------------------------------*/
/* Private constants ---------------------------------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------------------------------------*/
/* Global variables ----------------------------------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------------------------------------*/
volatile u16 	time = 0;
volatile u8 timProcessUart = 0, timeout10ms = 0, timeout100ms = 0, timeout1s = 0;
/* Private function -----------------------------------------------------------------------------*/
void sysClkCB(void)
{
	time++;
	timProcessUart++;
}

#define LED1_GPIOX                 B
#define LED1_GPION                 4

#define LED1_GPIO                  STRCAT2(GPIO_P,         LED1_GPIOX)
#define LED1_GPIO_PORT             STRCAT2(HT_GPIO,        LED1_GPIOX)
#define LED1_PIN                 	 STRCAT2(GPIO_PIN_,      LED1_GPION)
/* Global functions ----------------------------------------------------------------------------------------*/
/*********************************************************************************************************//**
  * @brief  Main program.
  * @retval None
  ***********************************************************************************************************/
int main(void)
{	
	uint8_t led1Stt = 0;
	CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};
	CKCUClock.Bit.PB = 1;
	CKCUClock.Bit.AFIO         = 1;
	CKCU_PeripClockConfig(CKCUClock, ENABLE);

  AFIO_GPxConfig(LED1_GPIO, LED1_PIN, AFIO_FUN_GPIO);
  GPIO_DirectionConfig(LED1_GPIO_PORT, LED1_PIN, GPIO_DIR_OUT);
	GPIO_WriteOutBits(LED1_GPIO_PORT, LED1_PIN, SET);
		
	UART_USART_Configuration();
	SysClockCfg_RegCallback(sysClkCB);
	DEBUG("Init done\r\n");
	
	while (1)                           /* Infinite loop                                                      */
  {
		if(timProcessUart >= 1)
		{
			timProcessUart = 0;
			timeout10ms++;
			UART_USART_Process();
		}
				
		if(timeout10ms >= 10)
		{
			timeout10ms = 0;
			timeout100ms++;
			
		}
		
		if(timeout100ms >= 10)
		{
			timeout100ms = 0;
			timeout1s++;
			
		}
		
		if(timeout1s >= 10)
		{
			timeout1s = 0;
			GPIO_WriteOutBits(LED1_GPIO_PORT, LED1_PIN, led1Stt);
			led1Stt = !led1Stt;
		}
  }
}

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */
