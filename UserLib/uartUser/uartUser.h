/*
 * uartUser.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef UARTUSER_H
#define UARTUSER_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht32.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
#define DEBUG_EN 1

#define RX_BUFFER_SIZE 20
#define MAX_NUM_DATA 100
typedef struct
{
	uint8_t Data[MAX_NUM_DATA];
	uint8_t Byte;
	uint8_t Index;
	uint8_t State;
} buffer_t;
/***************************  USART0  ***************************************//**/
#define USART0_TX_GPIOX                    A
#define USART0_TX_GPION                    2
#define USART0_RX_GPIOX                    A
#define USART0_RX_GPION                    3
#define USART0_IPN                         USART0

#define USART0_TX_GPIO_ID                  STRCAT2(GPIO_P,         USART0_TX_GPIOX)
#define USART0_RX_GPIO_ID                  STRCAT2(GPIO_P,         USART0_RX_GPIOX)
#define USART0_TX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      USART0_TX_GPION)
#define USART0_RX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      USART0_RX_GPION)
#define USART0_PORT                        STRCAT2(HT_,            USART0_IPN)
//#define USART0_IRQn                        
//#define USART0_IRQHandler                  
#define USART0_RX_GPIO_CLK                 STRCAT2(P,              USART0_RX_GPIOX)
#define USART0_RX_GPIO_PORT                STRCAT2(HT_GPIO,        USART0_RX_GPIOX)
#define USART0_RX_GPIO_PIN                 STRCAT2(GPIO_PIN_,      USART0_RX_GPION)

/***************************   UART0  ***************************************//**/
#define UART0_TX_GPIOX                    A
#define UART0_TX_GPION                    4
#define UART0_RX_GPIOX                    A
#define UART0_RX_GPION                    5
#define UART0_IPN                         UART0

#define UART0_TX_GPIO_ID                  STRCAT2(GPIO_P,         UART0_TX_GPIOX)
#define UART0_RX_GPIO_ID                  STRCAT2(GPIO_P,         UART0_RX_GPIOX)
#define UART0_TX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      UART0_TX_GPION)
#define UART0_RX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      UART0_RX_GPION)
#define UART0_PORT                        STRCAT2(HT_,            UART0_IPN)
//#define UART0_IRQn                        
//#define UART0_IRQHandler                  
#define UART0_RX_GPIO_CLK                 STRCAT2(P,              UART0_RX_GPIOX)
#define UART0_RX_GPIO_PORT                STRCAT2(HT_GPIO,        UART0_RX_GPIOX)
#define UART0_RX_GPIO_PIN                 STRCAT2(GPIO_PIN_,      UART0_RX_GPION)


#if(DEBUG_EN == 1)
#define DEBUG(str, ...) printf(str, ##__VA_ARGS__)
#else
#define DEBUG(str, ...)
#endif
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */





/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
/*************************************************************************************************************
  * @brief  Configure the UART
  * @retval None
  ***********************************************************************************************************/
void UART_USART_Configuration(void);

/*************************************************************************************************************
  * @brief  Transmit data
  * @retval None
  ***********************************************************************************************************/
void UART_USART_SendByte(HT_USART_TypeDef* USARTx, uint8_t xByte);
void UART_USART_SendString(HT_USART_TypeDef* USARTx, uint8_t *str);

/*************************************************************************************************************
  * @brief  Process Received data
  * @retval None
  ***********************************************************************************************************/
void UART_USART_Process(void);

/*************************************************************************************************************
  * @brief  Configuration and process function USART1 
  * @retval None
  ***********************************************************************************************************/
void UART_USART_PDMA_Configuration(void);
void UART_USART_PDMA_Process(void);
#endif
#ifdef __cplusplus
}
#endif
