/*
 * testFunction.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "ht32.h"
#include "uartUser.h"
#include "rtcInternal.h"
#include <string.h>
#include <stdlib.h>

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */
#define PACKET_TABLE_SIZE 2

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */

/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */
void Hdl_SetTime(uint8_t *str);
static void Hdl_JumpToNewAP(uint8_t *str);
/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
typedef struct 
{
	char packetStr[10];
	void (*func)(uint8_t *str);
} packetProcess_t;

packetProcess_t packetTable[PACKET_TABLE_SIZE] = 
{
	{"SET_TIME", Hdl_SetTime},	
	{"GOTO_AP",  Hdl_JumpToNewAP},	
};

/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
void User_uartHdl(uint8_t *str)
{
	uint8_t i;
	/* handle */
	for(i = 0; i < PACKET_TABLE_SIZE; i++)
	{
		if(strstr((const char*)str, (const char*)packetTable[i].packetStr) != NULL)
		{
			packetTable[i].func(str);
		}
	}
}

void Hdl_SetTime(uint8_t *str)
{
	uint8_t i = 0, strNum = 0, hour, minute, second;
	char *ptr[3];
	while(str[i] != '\0')
	{
		if(str[i] == ':') 
		{
			str[i] = '\0';
			ptr[strNum] = (char*)&str[i+1];
			strNum++;
		}
		i++;
	}

	hour 	 = atoi(ptr[0]);
	minute = atoi(ptr[1]);
	second = atoi(ptr[2]);
	
	RtcInternal_Time_Adjust(hour, minute, second, 28, 12, 2020);
}

#if defined (__CC_ARM)
/*********************************************************************************************************//**
  * @brief  Jump to user application by change PC.
  * @param  address: Start address of user application
  * @retval None
  ***********************************************************************************************************/
__asm void IAP_GoCMD(u32 address)
{
  LDR R1, [R0]
  MOV SP, R1
  LDR R1, [R0, #4]
  BX R1
}
#elif defined (__ICCARM__)
void IAP_GoCMD(u32 address)
{
  __asm("LDR R1, [R0]");
  __asm("MOV SP, R1");
  __asm("LDR R1, [R0, #4]");
  __asm("BX R1");
}
#endif

static void Hdl_JumpToNewAP(uint8_t *str)
{
	uint32_t add;
	
	uint8_t i = 0, strNum = 0;
	char *ptr[1];
	while(str[i] != '\0')
	{
		if(str[i] == ':') 
		{
			str[i] = '\0';
			ptr[strNum] = (char*)&str[i+1];
			strNum++;
		}
		i++;
	}
	add = atoi(ptr[0]);
	DEBUG("Jump: add [%d]\r\n", add);
	
	PDMA_EnaCmd(USART1_RX_PDMA_CH, DISABLE);
  NVIC_DisableIRQ(USART1_RX_PDMA_IRQ);
	USART_DeInit(USART1_PORT);      
	
	NVIC_DisableIRQ(BFTM0_IRQn);
	BFTM_DeInit(HT_BFTM0);
	
	NVIC_DisableIRQ(RTC_IRQn);
	RTC_DeInit();

  NVIC_SetVectorTable(NVIC_VECTTABLE_FLASH, add); /* Set Vector Table Offset                  */
  IAP_GoCMD(add);
}
