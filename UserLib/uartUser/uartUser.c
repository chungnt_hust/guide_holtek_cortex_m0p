/*
 * uartUser.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "uartUser.h"
#include <string.h>

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */

/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
buffer_t bufferRx[2];
HT_USART_TypeDef *uartPort[2] = {USART0_PORT, UART0_PORT};

/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */
static void RX_ProcessNewData(uint8_t uartId);

/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
void UART_USART_Configuration(void)
{
  { /* Enable peripheral clock of AFIO, UxART                                                               */
    CKCU_PeripClockConfig_TypeDef CKCUClock = {{0}};
    CKCUClock.Bit.AFIO                   				= 1;
//		CKCUClock.Bit.USART0_RX_GPIO_CLK            = 1;
//		CKCUClock.Bit.USART0_IPN		                = 1;
    CKCUClock.Bit.UART0_RX_GPIO_CLK             = 1;
		CKCUClock.Bit.UART0_IPN		                  = 1;
    
    CKCU_PeripClockConfig(CKCUClock, ENABLE);
  }

  /* Turn on UxART Rx internal pull up resistor to prevent unknow state                                     */
//	GPIO_PullResistorConfig(USART0_RX_GPIO_PORT, USART0_RX_GPIO_PIN, GPIO_PR_UP);                                
	GPIO_PullResistorConfig(UART0_RX_GPIO_PORT, UART0_RX_GPIO_PIN, GPIO_PR_UP);                                
	
  /* Config AFIO mode as UxART function.                                                                    */
//	AFIO_GPxConfig(USART0_TX_GPIO_ID, USART0_TX_AFIO_PIN, AFIO_FUN_USART_UART); // TxS0
//  AFIO_GPxConfig(USART0_RX_GPIO_ID, USART0_RX_AFIO_PIN, AFIO_FUN_USART_UART); // RxS0
	AFIO_GPxConfig(UART0_TX_GPIO_ID, UART0_TX_AFIO_PIN, AFIO_FUN_USART_UART); // Tx0
  AFIO_GPxConfig(UART0_RX_GPIO_ID, UART0_RX_AFIO_PIN, AFIO_FUN_USART_UART); // Rx0
  
	{
    /* UxART configured as follow:
          - BaudRate = 115200 baud
          - Word Length = 8 Bits
          - One Stop Bit
          - None parity bit
    */
    USART_InitTypeDef USART_InitStructure = {0};
    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WORDLENGTH_8B;
    USART_InitStructure.USART_StopBits = USART_STOPBITS_1;
    USART_InitStructure.USART_Parity = USART_PARITY_NO;
    USART_InitStructure.USART_Mode = USART_MODE_NORMAL;
//		USART_Init(USART0_PORT, &USART_InitStructure);
    USART_Init(UART0_PORT, &USART_InitStructure);
  }

  /* Enable UxART interrupt of NVIC                                                                         */
//	NVIC_EnableIRQ(USART0_IRQn);	
	NVIC_EnableIRQ(UART0_IRQn);	
	
  /* Enable UxART Rx interrupt                                                                              */
//	USART_IntConfig(USART0_PORT, USART_INT_RXDR, ENABLE);
	USART_IntConfig(UART0_PORT, USART_INT_RXDR, ENABLE);
	
  /* Enable UxART Tx and Rx function                                                                        */
//	USART_TxCmd(USART0_PORT, ENABLE);
//  USART_RxCmd(USART0_PORT, ENABLE);
	USART_TxCmd(UART0_PORT, ENABLE);
  USART_RxCmd(UART0_PORT, ENABLE);
}

void UART_USART_SendByte(HT_USART_TypeDef* USARTx, uint8_t xByte)
{
    USART_SendData(USARTx, xByte);
		while(USART_GetFlagStatus(USARTx, USART_FLAG_TXC) == RESET);
}

void UART_USART_SendString(HT_USART_TypeDef* USARTx, uint8_t *str)
{
    while(*str != 0)
    {
        UART_USART_SendByte(USARTx, *str);
        str++; 
    }
}

/*************************************************************************************************************
  * @brief  For UxART RX
  * @retval None
  ***********************************************************************************************************/
void UART_USART_Process(void)
{
	uint8_t i;
	for(i = 0; i < 2; i++)
	{
		if(bufferRx[i].State > 0)
		{
			bufferRx[i].State--;
			if(bufferRx[i].State == 0)
			{
				RX_ProcessNewData(i);
			}
		}
	}
}

/*************************************************************************************************************
  * @brief  IRQHandler
  * @retval None
  ***********************************************************************************************************/
void USART0_IRQHandler(void)
{
	if(USART_GetFlagStatus(uartPort[0], USART_FLAG_RXDR))
	{
			bufferRx[0].Data[bufferRx[0].Index] = USART_ReceiveData(uartPort[0]);
			bufferRx[0].Index++;
			bufferRx[0].State = 3;
	}
}


void UART0_IRQHandler(void)
{
	if(USART_GetFlagStatus(uartPort[1], USART_FLAG_RXDR))
	{
			bufferRx[1].Data[bufferRx[1].Index] = USART_ReceiveData(uartPort[1]);
			bufferRx[1].Index++;
			bufferRx[1].State = 3;
	}
}

static void RX_ProcessNewData(uint8_t uartId)
{
	bufferRx[uartId].Data[bufferRx[uartId].Index] = 0;
	DEBUG("Uart Rx: %s\r\n", bufferRx[uartId].Data);

	bufferRx[uartId].Index = 0;
}
