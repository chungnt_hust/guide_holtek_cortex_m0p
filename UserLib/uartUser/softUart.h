/*
 * softUart.h
 *
 *  Created on: Oct 8, 2019
 *      Author: chungnguyen
 */

#ifndef SOFTUART_SOFTUART_H_
#define SOFTUART_SOFTUART_H_
#include "ht32.h"


void delay_us(unsigned long time);					// delay microseconds
void SOFTUART_Configuration(void);
void SOFTUART_TransmitChar(const char DataValue);
void SOFTUART_TransmitString(const char* str);
void SOFTUART_TransmitStringValue(const char* str, ...);

#endif /* SOFTUART_SOFTUART_H_ */
